## Url Design
- **home**
  - Go to your boards (if logged in)
  - Login (if not logged in)
  - Sign up (if not logged in)
  - Public boards
- **home/signup**
  - email
  - full name
  - password
  - confirm password
- **home/login**
  - email
  - password
- **user/boards/create-first-board**
  - Name your board
  - Name a list
  - Add some cards
  - Card Description
  - Add a checklist to start
  - Get started (go to the board)
- **user/boards/board**
  - header
    - home-button (to my-boards)
    - profile-picture (showing current user) (generated dynamically)
  - name of the board
  - public/private
  - members
  - lists
  - add another list
  - cards in list
  - add another card in list
- **user/boards/board/card** 
  - Card name in list(some list)
  - Card description
  - Add a checklist
  - add item to checklist
  - Comments
- **user/boards/board/members**
  - Board Admin
  - Add member
  - Remove member
- **user/account**
  - edit email
  - change password
  - update profile pic

## Forms
- All forms
  - Home
    - Login Form
    - Sign up Form
  - Board
    - Create first board
    - Add a list
    - Add a card
    - Rename list
    - Rename card
    - Remove a list - Removes all cards and checklist in it
    - Remove a card - Removes all checklist in it
  - Card
    - Add a checklist
    - Add an item in checklist
    - Rename checklist
    - Edit list-item in checklist
    - Remove a checklist - Removes all the list items
    - Remove a list-item
  - Members
    - Add members of a board (if board is private) (Only admin of the board can add or remove members)
  - User-account
    - edit email of the user
    - change password of the user
    - update the profile pic of the user

## Database Schema

- List of tables in database
  - Users
  - Boards
  - Cards
  - Checklists
  - Checklist-items
  - Board-members
  
- Users  
  
  | User id(PK) | email   | password | profile-picture |
  | ----------- | ------- | -------- | --------------- |
  | 1           | a@a.com | qeqweqd  | a.jpeg          |

- Boards
  
  | User id(FK) | Board id(PK) | Board name | Board Description      | Is private |
  | ----------- | ------------ | ---------- | ---------------------- | ---------- |
  | 1           | 1            | My Board   | This is my first Board | True       |


- Cards

    | List id(FK) | Card id(PK) | Card name | Card description           |
    | ----------- | ----------- | --------- | -------------------------- |
    | 2           | 2           | My Card   | this card has that purpose |

- Checklists

    | Card id(FK) | Checklist id(PK) | Checklist name |
    | ----------- | ---------------- | -------------- |
    | 2           | 4                | My Checklist   |

- Checklist Items

    | Checklist id(FK) | Item id(PK) | Item content    | Is checked |
    | ---------------- | ----------- | --------------- | ---------- |
    | 2                | 1           | Something to do | True       |

- Board Members

    | Board id(PK) | User id(FK) | is Admin |
    | ------------ | ----------- | -------- |
    | 2            | 1           | False    |

## User Stories

- As a user I should be able to create boards, so that I can group the similar cards.
- As a user I should be update the boards, so that I can change the Board Info.
- As a user I should be delete boards, so that I can remove the unused boards.

- As a user I should be able to create cards, so that I can group the similar checklist.
- As a user I should be able to update cards, so that I can change the card Info.
- As a user I should be able to delete cards, so that I can remove unused cards.
  
- As a user I should be able to create checklist, so that I can group the similar lists.
- As a user I should be able to update checklist, so that I can change the checklist Info.
- As a user I should be able to delete checklist, so that I can remove unused checklists.
  
- As a user I should be able to collaborate with other members, so that I can work on the same board.
- As an admiin I should be able to add and remove members to the board, So that i can maintain authorization.

- As a user I should be able to create public boards, so that I can share it publicly.
- As a user I should be able to create private boards, so that I can maintain authorization.
