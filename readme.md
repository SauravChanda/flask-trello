# Getting Flask App Running
- git clone <repository>
- `pip install -r requirements.py`
- Install postgres
- create flask-trello database in postgres

# Environment Variables
- create config.json in /etc/
- create all environment variables there
- the environment variable `'ENABLE_SEARCH'` toggles between elastic search activation

# Flask run
- `Flask run` to successfully run the app

# Install reddis
- `sudo apt install build-essential`
- `wget -c http://download.redis.io/redis-stable.tar.gz`
- `tar -xvzf redis-stable.tar.gz`
- `cd redis-stable`
- `make` 
- `make test`
- `sudo make install`

# Install celery
- `pip3 install celery`

# Using Supervisor to handle background task
- `Sudo apt install supervisor`
- write supervisor conf in /etc/supervisor/conf.d/trello.conf
- create the error log and access log in /var/log/trello/
- Restart supervisor with `sudo supervisorctl reload`