## Supervisor config

[program:trello]
directory=/home/ubuntu/flask-trello
command=gunicorn3 Trello:app
user=ubuntu
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
stderr_logfile=/var/log/trello/trello.err.log
stdout_logfile=/var/log/trello/trello.out.log

[program:reddis]
directory=/home/ubuntu/flask-trello/redis-stable/src
command=redis-server
user=ubuntu
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
stderr_logfile=/var/log/trello/reddis.err.log
stdout_logfile=/var/log/trello/reddis.out.log

[program:celery]
directory=/home/ubuntu/flask-trello
command=celery -A app.celery worker
user=ubuntu
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
stderr_logfile=/var/log/trello/celery.err.log
stdout_logfile=/var/log/trello/celery.out.log

- Created a key pair
    - Name: Saurav

- ~/.bash_profile (To save environment variables)

- using supervisor
    - sudo nano /etc/supervisor/conf.d/trello.conf