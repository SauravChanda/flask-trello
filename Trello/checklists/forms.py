from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class AddAChecklistForm(FlaskForm):
    name = StringField('Add a new checklist', validators=[
                       Length(max=100), DataRequired()])
    submit = SubmitField('Add')


class AddAChecklistItemForm(FlaskForm):
    name = StringField('Add a new checklist', validators=[
                       Length(max=100), DataRequired()])
    submit = SubmitField('Add')
