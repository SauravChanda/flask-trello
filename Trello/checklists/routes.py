from flask import Blueprint, abort
from Trello import db
from flask import render_template, url_for, request, redirect, flash
from flask_login import current_user
from Trello.checklists.forms import AddAChecklistForm
from Trello.models import Cards, Checklist, ChecklistItems
from Trello.authorization import second_level_authorization, second_level_authorization_can_edit


checklists = Blueprint('checklists', __name__)


def add_a_checklist_in_the_database(card, form):
    checklist = Checklist(card_id=card.id, name=form.name.data)
    db.session.add(checklist)
    db.session.commit()
    return checklist


@checklists.route("/<user>/boards/<board>/cards/<card>", methods=['GET', 'POST'])
def card(user, board, card):
    can_edit = False
    form = AddAChecklistForm()
    card = Cards.query.filter_by(id=card).first()
    if card:
        if second_level_authorization(card.board.id):
            if request.method == 'POST' and form.validate():
                add_a_checklist_in_the_database(card, form)
                flash(f"addeded checklist {form.name.data}", 'success')
                return redirect(url_for('checklists.card', user=user, board=board, card=card.id))
        else:
            abort(400)
    else:
        abort(404)
    if second_level_authorization_can_edit(card.board.id):
        can_edit = True
    return render_template('card.html', page_title='card', card=card, form=form, can_edit=can_edit)


def add_a_checklist_item_in_the_database():
    response = request.get_json()
    checklist_item = ChecklistItems(
        checklist_id=response['checklistId'], content=response['checklistInput'])
    db.session.add(checklist_item)
    db.session.commit()
    return checklist_item


@checklists.route("/<user>/boards/<board>/cards/<card>/add-checklist-item", methods=['GET', 'POST'])
def add_a_checklist_item(user, board, card):
    card = Cards.query.filter_by(id=card).first()
    if second_level_authorization(card.board.id):
        checklist_item = add_a_checklist_item_in_the_database()
        response = str(checklist_item.id)
        return response
    else:
        abort(404)


def update_a_checklist_item_in_database():
    response = request.get_json()
    checklist = ChecklistItems.query.filter_by(
        id=response['checklistItemId']).first()
    checklist.is_checked = response['is_checked']
    db.session.commit()


@checklists.route("/<user>/boards/<board>/cards/<card>/update-checklist-item", methods=['GET', 'POST'])
def update_a_checklist_item(user, board, card):
    card = Cards.query.filter_by(id=card).first()
    if second_level_authorization(card.board.id):
        update_a_checklist_item_in_database()
        return "OK"
    else:
        abort(400)


def delete_a_checklist_from_the_database():
    response = request.get_json()
    print(response['checklistId'])
    checklist = Checklist.query.filter_by(
        id=response['checklistId']).first()
    print(checklist)
    db.session.delete(checklist)
    db.session.commit()


@checklists.route("/<user>/boards/<board>/cards/<card>/delete-checklist", methods=['DELETE'])
def delete_a_checklist(user, board, card):
    card = Cards.query.filter_by(id=card).first()
    if second_level_authorization(card.board.id):
        delete_a_checklist_from_the_database()
        return "OK"
    else:
        abort(404)


def delete_a_checklist_item_from_the_database():
    response = request.get_json()
    print(response['checklistItemId'])
    checklist_item = ChecklistItems.query.filter_by(
        id=response['checklistItemId']).first()
    db.session.delete(checklist_item)
    db.session.commit()


@checklists.route("/<user>/boards/<board>/cards/<card>/delete-checklist-item", methods=['DELETE'])
def delete_a_checklist_item(user, board, card):
    card = Cards.query.filter_by(id=card).first()
    if second_level_authorization(card.board.id):
        delete_a_checklist_item_from_the_database()
        return "OK"
    else:
        abort(404)
