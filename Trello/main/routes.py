from flask import Blueprint
from flask import render_template
from Trello.models import Boards, Users
from Trello import es, celery, redis_client
from flask import request, redirect, url_for
from flask_login import current_user
from Trello.services import fetch_all_shared_boards


main = Blueprint("main", __name__)


@main.route("/")
def home():
    if current_user.is_authenticated:
        return redirect(url_for("boards.my_boards", user=current_user.username))
    else:
        boards = Boards.query.filter_by(is_private=False).all()
        return render_template("home.html", page_title="home", boards=boards)


@celery.task()
def fetch_user_data(user_id):
    all_user_boards = str(Boards.query.filter_by(user_id=user_id).all())
    all_shared_boards = str(fetch_all_shared_boards(user_id))
    user_information = str(Users.query.filter_by(id=user_id).first())

    return [all_shared_boards, all_user_boards, user_information]


@main.route("/<username>/download_data")
def download_user_data(username):
    user_data = fetch_user_data.apply_async(args=[current_user.id], countdown=10)
    return user_data.id


@main.route("/data_status/<task_id>")
def status_data(task_id):
    user_data = fetch_user_data.AsyncResult(task_id)
    return user_data.status


@main.route("/get_data/<task_id>")
def get_data(task_id):
    user_data = redis_client.get("celery-task-meta-" + task_id)
    redis_client.delete("celery-task-meta-" + task_id)
    return user_data


@main.route("/search", methods=["GET", "POST"])
def search():
    params = request.get_json()
    if not params:
        params = {}
        params["searchKeyword"] = request.args.get("q")
    print(params["searchKeyword"])
    if current_user.is_authenticated:
        query = {
            "query": {
                "dis_max": {
                    "queries": [
                        {
                            "bool": {
                                "must": [
                                    {
                                        "match_phrase_prefix": {
                                            "name": params["searchKeyword"]
                                        }
                                    },
                                    {"term": {"members": current_user.id}},
                                ]
                            }
                        },
                        {
                            "bool": {
                                "must": [
                                    {
                                        "match_phrase_prefix": {
                                            "name": params["searchKeyword"]
                                        }
                                    },
                                    {"match": {"is_private": "false"}},
                                ]
                            }
                        },
                    ],
                    "tie_breaker": 0.7,
                }
            }
        }
        searched = es.search(body=query)
    else:
        query = {
            "query": {
                "bool": {
                    "must": [
                        {"match_phrase_prefix": {"name": params["searchKeyword"]}},
                        {"match": {"is_private": "false"}},
                    ]
                }
            }
        }
        searched = es.search(body=query)
    return searched
