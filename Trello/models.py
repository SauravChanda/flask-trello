from Trello import db, login_manager
from flask_login import UserMixin


@login_manager.user_loader
def load_user(id):
    return Users.query.get(int(id))


class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), nullable=False, unique=True)
    email = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(60), nullable=False)
    profile_picture = db.Column(
        db.String(120), nullable=False, default='default.jpg')
    boards = db.relationship('Boards', backref='owner',
                             lazy=True, cascade='all, delete-orphan')

    def __repr__(self):
        return f"User({self.id}, {self.email},\
                      {self.profile_picture}, {self.boards})"


class Boards(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey(
        'users.id'), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    is_private = db.Column(db.Boolean, nullable=False, default=True)
    cards = db.relationship('Cards', backref='board',
                            lazy=True, cascade='all, delete-orphan')
    members = db.relationship(
        'BoardMembers', backref='members_board', lazy=True, cascade='all, delete-orphan')

    def __repr__(self):
        return f"Boards({self.user_id}, {self.id},\
                        {self.name}, {self.description},\
                        {self.is_private}, {self.cards})"


class BoardMembers(db.Model):
    board_id = db.Column(db.Integer, db.ForeignKey(
        'boards.id'), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False)
    is_admin = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return f"BoardMembers({self.member_id}, {self.id},\
                              {self.is_admin})"


class Cards(db.Model):
    board_id = db.Column(db.Integer, db.ForeignKey(
        'boards.id'), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    checklists = db.relationship(
        'Checklist', backref='card', lazy=True, cascade='all, delete-orphan')

    def __repr__(self):
        return f"Cards({self.board_id}, {self.id},\
                       {self.name}, {self.description},\
                       {self.checklists})"


class Checklist(db.Model):
    card_id = db.Column(db.Integer, db.ForeignKey(
        'cards.id'), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    checklist_items = db.relationship(
        'ChecklistItems', backref='checklist', lazy=True, cascade='all, delete-orphan')

    def __repr__(self):
        return f"Checklist({self.card_id}, {self.id},\
                           {self.name}, {self.checklist_items})"


class ChecklistItems(db.Model):
    checklist_id = db.Column(
        db.Integer, db.ForeignKey('checklist.id'), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False)
    is_checked = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return f"ChecklistItems({self.checklist_id}, {self.id},\
                                {self.content})"
