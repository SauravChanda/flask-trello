removeBoardMembersButtons = document.getElementsByClassName('remove-member-btn')
url = window.location.href

for (let i = 0; i < removeBoardMembersButtons.length; i++) {
    removeBoardMembersButton = removeBoardMembersButtons[i]
    removeBoardMembersButton.setAttribute('onclick', `{removeBoardMember('${removeBoardMembersButton.id}')}`)
}

function removeBoardMember(id) {
    id_arr = id.split('-')
    userId = id_arr[1]
    boardId = id_arr[2]
    a = {
        'userId': userId,
        'boardId': boardId
    }
    response = fetch(url + '/remove-board-member',
        {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(a)
        }
    )
    response.then(response => {
       location.reload()
    })
}