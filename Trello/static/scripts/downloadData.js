let downloadButton = document.getElementById('download-button')
downloadButton.addEventListener('click', checkIfDataPreparationIsDone)

async function checkIfDataPreparationIsDone() {
    downloadButton.outerHTML = '<button id="download-progress" class="btn btn-secondary" type="button" disabled>\
    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>\
    <span class="sr-only">Loading...</span>\
    Preparing your data please wait..\
    </button>'
    url = `/${downloadButton.value}/download_data`
    response = await fetch(url, {
        method: "GET"
    })
    response.text().then(response => {
        console.log(response)
        checkStatus(response)
    })
}

async function checkStatus(id) {
    let checkUrl = `/data_status/${id}`
    status = ""
    while (status != "SUCCESS" && status != "FAILURE") {
        await new Promise(r => setTimeout(r, 2000))
        response = await fetch(checkUrl, {
            method: 'GET'
        })
        response.text().then(response => {
            console.log(status)
            status = response
        })
    }
    if (status == "SUCCESS") {
        getdata(id)
    }

}

async function getdata(id) {
    let getUrl = `/get_data/${id}`

    response = await fetch(getUrl, {
        method: 'GET'
    })
    response.text().then(response => {
        console.log(response)
        download("hello.txt", response);

    })
}

function download(filename, text) {
    let downloadProgress = document.getElementById("download-progress")
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
    downloadProgress.outerHTML = '<button id="download-button" value="{{ current_user.username }}"\
                                class="btn btn-success">Download My Data</button>'

    downloadButton = document.getElementById('download-button')
    downloadButton.addEventListener('click', checkIfDataPreparationIsDone)
}



