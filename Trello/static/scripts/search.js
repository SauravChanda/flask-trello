searchBar = document.getElementById('search-bar')
searchBar.addEventListener('keyup', search)
let prev_time = new Date().getSeconds()
searchResultsDisplay = document.getElementById('search-results')
search = document.getElementById('search')
search.addEventListener('mouseleave', hide)
async function search() {
    body = {
        'searchKeyword': searchBar.value
    }
    response = await fetch("/search", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
    })
    searchResultsDisplay.innerHTML = '<li class="list-group-item">No results</li>'
    response.text().then(response => {
        searchHits = JSON.parse(response).hits.hits
        if (searchHits.length > 0) {
            searchResultsDisplay.innerHTML = ''
        }
        searchHits.forEach(hits => {
            if (hits._index == 'cards') {
                searchResultsDisplay.innerHTML += `<a href=/${hits._source.url} class='list-group-item list-group-item-action'>${hits._source.name} <span class="badge badge-warning float-right">Card</span></a>`
            } else if (hits._index == 'boards') {
                searchResultsDisplay.innerHTML += `<a href=/${hits._source.url} class='list-group-item list-group-item-action'>${hits._source.name} <span class="badge badge-dark float-right">Board</span></a>`
            }
        });
    })


}

function hide() {
    console.log(event.type)
    console.log("hi")
    searchResultsDisplay.innerHTML = ''

}

