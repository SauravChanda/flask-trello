checklistInputs = document.getElementsByClassName('checklist-input')
checklistButtons = document.getElementsByClassName('checklist-button')
checklistCheckboxes = document.getElementsByClassName('checklist-checkbox')
deleteChecklistButtons = document.getElementsByClassName('delete-checklist-button')
deleteChecklistItemButtons = document.getElementsByClassName('delete-checklist-item-button')
url = window.location.href
url_parts = url.split('/')
for (let i = 0; i < checklistButtons.length; i++) {
    checklistButton = checklistButtons[i]
    checklistButton.setAttribute('onclick', `addItemToAChecklist('${checklistButton.id}')`)
    deleteChecklistButton = deleteChecklistButtons[i]
    deleteChecklistButton.setAttribute('onclick', `{deleteAChecklist('${deleteChecklistButton.id}')}`)
}
for (let i = 0; i < checklistCheckboxes.length; i++) {
    checklistCheckbox = checklistCheckboxes[i]
    checklistCheckbox.setAttribute('onclick', `{updateChecklist('${checklistCheckbox.id}')}`)
    deleteChecklistItemButton = deleteChecklistItemButtons[i]
    deleteChecklistItemButton.setAttribute('onclick', `{deleteAChecklistItem('${deleteChecklistItemButton.id}')}`)
}
function addItemToAChecklist(id) {
    id = id.split('-')
    id = id[2]
    let checklistInput = document.getElementById(`checklist-${id}-input`)
    if (checklistInput.value != '') {
        checklistItemList = document.getElementById(`checklist-${id}-items`)
        a = {
            'checklistInput': checklistInput.value,
            'checklistId': id
        }
        response = fetch(url + '/add-checklist-item',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(a)
            }
        )
        response.then(response => {
            war = response.text().then(
                response => {
                    console.log(response)
                    let HTMLContent = `<li id="checklist-item-${response}"\
                                    class="list-group-item d-flex flex-row wrap-nowrap justify-content-between align-items-center">\
                                    <div>\
                                    <input type="checkbox" class="checklist-checkbox" id="${id}-${response}">\
                                    &nbsp ${checklistInput.value}\
                                    </div>\
                                    <a class="btn btn-sm btn-danger delete-checklist-item-button"\
                                    id="delete-${id}-${response}">Delete</a>\
                                    </li>`
                    checklistItemList.innerHTML += HTMLContent
                    checklistInput.value = ''
                    checklistCheckbox = document.getElementById(`${id}-${response}`)
                    checklistCheckbox.setAttribute('onclick', `{updateChecklist('${checklistCheckbox.id}')}`)
                    checklistDelete = document.getElementById(`delete-${id}-${response}`)
                    console.log(checklistDelete)
                    checklistDelete.setAttribute('onclick', `{deleteAChecklistItem('${checklistDelete.id}')}`)
                }
            )
        })
    }
}


function updateChecklist(id) {
    console.log(id)
    checklistCheckbox = document.getElementById(id)
    is_checked = checklistCheckbox.checked
    id = id.split('-')
    checklistId = id[0]
    checklistItemId = id[1]
    console.log(checklistId)
    a = {
        'is_checked': is_checked,
        'checklistId': checklistId,
        'checklistItemId': checklistItemId
    }
    response = fetch(url + '/update-checklist-item',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(a)
        }
    )
    response.then(response => {
        console.log('Yayy')
    })
}

function deleteAChecklist(id) {
    id = id.split('-')
    id = id[2]
    console.log(id)
    a = {
        'checklistId': id
    }
    response = fetch(url + '/delete-checklist',
        {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(a)
        }
    )
    response.then(response => {
        checklist = document.getElementById(`checklist-${id}`)
        checklist.remove()
    })
}

function deleteAChecklistItem(id) {
    console.log(id)
    id = id.split('-')
    checklistId = id[1]
    checklistItemId = id[2]

    a = {
        'checklistId': checklistId,
        'checklistItemId': checklistItemId

    }
    response = fetch(url + '/delete-checklist-item',
        {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(a)
        }
    )
    response.then(response => {
        console.log(`checklist-item-${checklistItemId}`)
        checklist = document.getElementById(`checklist-item-${checklistItemId}`)
        checklist.remove()
    })
}