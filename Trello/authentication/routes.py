from flask import Blueprint
from Trello import bcrypt, db
from flask import render_template, url_for, request, redirect, flash
from flask_login import logout_user, login_required, login_user, current_user
from Trello.authentication.forms import LoginForm, RegisterForm
from Trello.models import Users

authentication = Blueprint('authentication', __name__)


@authentication.route("/signup", methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('boards.my_boards', user=current_user.username))
    form = RegisterForm()
    if request.method == 'POST' and form.validate():
        hashed_password = bcrypt.generate_password_hash(
            form.password.data).decode('utf-8')
        user = Users(username=form.username.data,
                     email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(f'Wellcome {form.username.data}', 'success')
        login_user(user)
        return redirect(url_for('boards.my_boards', user=user.username))
    return render_template('signup.html', page_title='signup', form=form)


@authentication.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('boards.my_boards', user=current_user.username))
    form = LoginForm()
    if request.method == 'POST' and form.validate():
        user = Users.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('boards.my_boards', user=user.username))
        else:
            flash("Either the email or the password was Incorrect please try again",'warning')
    return render_template('login.html', page_title='login', form=form)


@authentication.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('authentication.login'))
