import json

with open("/etc/config.json") as config_file:
    config = json.load(config_file)

SECRET_KEY = config.get("SECRET_KEY")
SQLALCHEMY_DATABASE_URI = config.get("SQLALCHEMY_DATABASE_URI")
FLASK_APP = config.get("FLASK_APP")
SERVER_NAME = config.get("SERVER_NAME")
CELERY_BROKER_URL = config.get("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = config.get("CELERY_RESULT_BACKEND")
SQLALCHEMY_TRACK_MODIFICATIONS = False
