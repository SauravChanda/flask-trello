from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextField
from wtforms.validators import DataRequired, Length


class AddACardForm(FlaskForm):
    name = StringField('Add a new card', validators=[
                       Length(max=100), DataRequired()])
    description = TextField('Add a card description')
    submit = SubmitField('Add')


class UpdateACardForm(FlaskForm):
    name = StringField('Add a new card', validators=[
                       Length(max=100), DataRequired()])
    description = TextField('Add a card description')
    submit = SubmitField('Update')

