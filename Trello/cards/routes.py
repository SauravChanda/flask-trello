from flask import Blueprint, abort
from Trello import app, db, es
from flask import render_template, url_for, request, redirect, flash
from flask_login import current_user, login_required
from Trello.cards.forms import AddACardForm, UpdateACardForm
from Trello.services import fetch_all_board_members_info, check_if_search_is_enabled
from Trello.models import Boards, Cards, Checklist, ChecklistItems
from Trello.authorization import (
    first_level_authorization,
    second_level_authorization,
    second_level_authorization_can_edit,
    third_level_authorization,
)


cards = Blueprint("cards", __name__)
search_enabled = check_if_search_is_enabled()


def add_a_card_to_the_database(a_board, form):
    card = Cards(
        board_id=a_board.id, name=form.name.data, description=form.description.data
    )
    db.session.add(card)
    db.session.commit()
    if search_enabled:
        add_a_card_to_es(card)
    return card


def get_board_members_id_in_array(board_id):
    array_of_all_member_id = []
    all_board_members_info, board_members_name = fetch_all_board_members_info(board_id)
    for board_member in all_board_members_info:
        print(board_member["board_member_info"])
        array_of_all_member_id.append(board_member["board_member_info"].id)

    return array_of_all_member_id


def add_a_card_to_es(card):
    the_card = {
        "board_id": card.board_id,
        "name": card.name,
        "description": card.description,
        "url": f"{card.board.owner.username}/boards/{card.board.id}/cards/{card.id}",
        "members": get_board_members_id_in_array(card.board.id),
    }

    es.index(index="cards", id=int(card.id), body=the_card)


@cards.route("/<user>/boards/<board>", methods=["GET", "POST"])
def my_board(user, board):
    can_edit = False
    a_board = Boards.query.filter_by(id=board).first()
    if a_board:
        if second_level_authorization(board):
            form = AddACardForm()
            if request.method == "POST" and form.validate():
                card = add_a_card_to_the_database(a_board, form)
                flash("Added a new card", "success")
                return redirect(
                    url_for("checklists.card", user=user, board=board, card=card.id)
                )
            if second_level_authorization_can_edit(board):
                can_edit = True
            return render_template(
                "board.html",
                page_title="board",
                board=a_board,
                form=form,
                can_edit=can_edit,
            )
        else:
            abort(400)
    else:
        abort(404)


def delete_a_card_from_the_database(the_card):
    db.session.delete(the_card)
    db.session.commit()
    if search_enabled:
        delete_a_card_from_es(the_card)


def delete_a_card_from_es(the_card):
    es.delete(index="cards", id=int(the_card.id))


@cards.route("/delete-card/<card>")
@login_required
def delete_card(card):
    the_card = Cards.query.get(card)
    if the_card:
        if second_level_authorization(the_card.board.id):
            delete_a_card_from_the_database(the_card)
            flash(f"removed card {the_card.name}", "success")
            return redirect(
                url_for(
                    "cards.my_board",
                    user=current_user.username,
                    board=the_card.board.id,
                )
            )
        else:
            abort(400)
    else:
        abort(404)


def update_a_card_in_database(the_card, form):
    the_card.name = form.name.data
    the_card.description = form.description.data
    db.session.commit()
    if search_enabled:
        update_a_card_in_es(the_card, form)


def update_a_card_in_es(the_card, form):
    a_card = {
        "name": form.name.data,
        "description": form.description.data,
    }
    es.index(index="cards", id=int(the_card.id), body=a_card)


def update_a_card_form_filler(the_card, form):
    form.name.data = the_card.name
    form.description.data = the_card.description


@cards.route("/update-card/<card>", methods=["GET", "POST"])
@login_required
def update_card(card):
    the_card = Cards.query.get(card)
    if the_card:
        if second_level_authorization(the_card.board.id):
            form = UpdateACardForm()
            if request.method == "POST" and form.validate():
                update_a_card_in_database(the_card, form)
                flash(f"updated card {the_card.name}", "success")
                return redirect(
                    url_for(
                        "cards.my_board",
                        user=current_user.username,
                        board=the_card.board.id,
                    )
                )
            update_a_card_form_filler(the_card, form)
            return render_template("update-card.html", form=form)
        else:
            abort(400)
    else:
        abort(404)


@cards.route("/<user>/boards/<board>/cards", methods=["GET", "POST"])
def redirect_to_board(user, board):
    return redirect(url_for("cards.my_board", user=user, board=board))
