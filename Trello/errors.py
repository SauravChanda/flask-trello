from Trello import app, db
from flask import render_template


@app.errorhandler(404)
def not_found(e):
    return render_template('not-found.html'), 404


@app.errorhandler(400)
def bad_request(e):
    return render_template('bad-request.html'), 400


@app.errorhandler(500)
def another_bad_request(e):
    db.session.rollback()
    return render_template('internal-server-error.html'), 500
