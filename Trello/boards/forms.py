from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextField, BooleanField
from wtforms.validators import DataRequired, Length


class AddABoardForm(FlaskForm):
    name = StringField('Add a new board', validators=[
                       Length(max=100), DataRequired()])
    description = TextField('Add a board description')
    is_private = BooleanField('is the board private??')
    submit = SubmitField('Add')


class UpdateABoardForm(FlaskForm):
    name = StringField('Add a new board', validators=[
                       Length(max=100), DataRequired()])
    description = TextField('Add a board description')
    is_private = BooleanField('is the board private??')
    submit = SubmitField('Update')
