from flask import Blueprint, abort
from Trello import app, db, es
from flask import render_template, url_for, request, redirect, flash
from flask_login import current_user, login_required
from Trello.boards.forms import AddABoardForm, UpdateABoardForm
from Trello.models import Boards, BoardMembers
from Trello.authorization import third_level_authorization, second_level_authorization
from Trello.services import fetch_all_shared_boards, check_if_search_is_enabled

boards = Blueprint("boards", __name__)
search_enabled = check_if_search_is_enabled()


def add_a_board_into_the_database(form):
    board = Boards(
        user_id=current_user.id,
        name=form.name.data,
        description=form.description.data,
        is_private=form.is_private.data,
    )
    board.members.append(BoardMembers(member_id=current_user.id, is_admin=True))
    db.session.add(board)
    db.session.commit()
    print(search_enabled, "search_enabled")
    if search_enabled:
        print('ho')
        add_a_board_into_es(board)
    return board


def add_a_board_into_es(board):
    a_board = {
        "user_id": board.user_id,
        "name": board.name,
        "description": board.description,
        "is_private": board.is_private,
        "url": f"{board.owner.username}/boards/{board.id}",
        "members": [current_user.id],
    }
    es.index(index="boards", id=int(board.id), body=a_board)


def fetch_all_boards():
    all_boards = Boards.query.filter_by(user_id=current_user.id).all()
    return all_boards


@boards.route("/<user>/boards", methods=["GET", "POST"])
@login_required
def my_boards(user):
    form = AddABoardForm()
    if request.method == "POST" and form.validate():
        board = add_a_board_into_the_database(form)
        flash(
            f'Added board "{form.name.data}"..... click the board to view more',
            "success",
        )
        return redirect(
            url_for("cards.my_board", user=current_user.username, board=board.id)
        )
    all_boards = fetch_all_boards()
    all_shared_boards = fetch_all_shared_boards(current_user.id)
    return render_template(
        "my-boards.html",
        page_title="my board",
        form=form,
        all_boards=all_boards,
        all_shared_boards=all_shared_boards,
    )


@boards.route("/<user>/boards/create-first-board")
@login_required
def create_first_board(user):
    return render_template("create-first-board.html", page_title="create first board")


def remove_a_board_from_the_database(the_board):
    db.session.delete(the_board)
    if search_enabled:
        delete_a_board_from_es(the_board)
    db.session.commit()


def delete_a_board_from_es(the_board):
    es.delete(index="boards", id=int(the_board.id))
    query = {"query": {"match": {"board_id": the_board.id}}}
    es.delete_by_query(index="cards", body=query)


@boards.route("/delete-board/<board>")
def delete_board(board):
    the_board = Boards.query.get(board)
    if the_board:
        if second_level_authorization(the_board.id):
            remove_a_board_from_the_database(the_board)
            flash(f"removed board {the_board.name}", "success")
            return redirect(url_for("boards.my_boards", user=current_user.username))
        else:
            abort(400)
    else:
        abort(404)


def update_board_in_database(the_board, form):
    the_board.name = form.name.data
    the_board.description = form.description.data
    the_board.is_private = form.is_private.data
    db.session.commit()
    if search_enabled:
        update_a_board_into_es(the_board, form)


def update_a_board_into_es(board, form):
    the_board = es.get(index="boards", id=int(board.id))
    the_board["_source"]["name"] = form.name.data
    the_board["_source"]["description"] = form.description.data
    the_board["_source"]["is_private"] = form.is_private.data
    body = the_board["_source"]
    es.index(index="boards", id=int(board.id), body=body)


def set_update_board_form_text(the_board, form):
    form.name.data = the_board.name
    form.description.data = the_board.description
    form.is_private.data = the_board.is_private


@boards.route("/update-board/<board>", methods=["GET", "POST"])
@login_required
def update_board(board):
    the_board = Boards.query.get(board)
    if the_board:
        if second_level_authorization(the_board.id):
            form = UpdateABoardForm()
            if request.method == "POST" and form.validate():
                update_board_in_database(the_board, form)
                flash(f"updated board {the_board.name}", "success")
                return redirect(url_for("boards.my_boards", user=current_user.username))
            set_update_board_form_text(the_board, form)
            return render_template("update-board.html", form=form)
        else:
            abort(400)
    else:
        abort(404)
