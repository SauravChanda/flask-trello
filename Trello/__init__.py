from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from elasticsearch import Elasticsearch
from flask_celery import make_celery
from flask_redis import FlaskRedis

app = Flask(__name__)
app.config.from_pyfile('config.py')

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "authentication.login"
es = Elasticsearch("http://localhost:9200")
celery = make_celery(app)
redis_client = FlaskRedis(app)


from Trello.authentication.routes import authentication
from Trello.users.routes import users
from Trello.boards.routes import boards
from Trello.cards.routes import cards
from Trello.checklists.routes import checklists
from Trello.members.routes import members
from Trello.main.routes import main
from Trello.errors import not_found, bad_request, another_bad_request

app.register_blueprint(authentication)
app.register_blueprint(users)
app.register_blueprint(boards)
app.register_blueprint(cards)
app.register_blueprint(checklists)
app.register_blueprint(members)
app.register_blueprint(main)
