# There will be four levels of authorization
# 1st-level - user can see the public board but cant do anything
#     - can be un_authenticated
# 2nd-level - user can access others board that is shared to them and can modify the cards and checklists
#     - has to be authenticated and be the member of that board
# 3rd-level - users can add or remove members from the board
#     - has to be the admin of that board

from Trello import app, db
from flask import render_template, url_for, request, redirect, flash
from flask_login import current_user, login_required
from Trello.models import Boards, BoardMembers


def first_level_authorization():
    return True


def second_level_authorization(board_id):
    board = Boards.query.filter_by(id=board_id).first()
    if board.is_private:
        if current_user.is_authenticated:
            board_member = BoardMembers.query.filter_by(
                member_id=current_user.id, board_id=board_id).first()
            print(board_member)
            if board_member:
                return True
            else:
                return False
    else:
        return True


def second_level_authorization_can_edit(board_id):
    board = Boards.query.filter_by(id=board_id).first()
    if current_user.is_authenticated:
        board_member = BoardMembers.query.filter_by(
            member_id=current_user.id, board_id=board_id).first()
        if board_member:
            return True
        else:
            return False
    else:
        return False


def third_level_authorization(board_id):
    if current_user.is_authenticated:
        board_member = BoardMembers.query.filter_by(
            member_id=current_user.id, board_id=board_id).first()
        print(board_member)
        if board_member:
            if board_member.is_admin:
                return True
            else:
                return False
        else:
            return False
    else:
        return False
