from flask import Blueprint
from flask import render_template
from flask_login import login_required


users = Blueprint('users', __name__)


@users.route("/<user>/account")
def account(user):
    return render_template("account.html")
