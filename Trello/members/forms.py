from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField
from wtforms.validators import DataRequired


class AddAMemberForm(FlaskForm):
    add_member = SelectField('Add a new member', choices=[])
    submit = SubmitField('Add')
