from flask import Blueprint, render_template, request, redirect, url_for, flash, abort
from Trello.models import BoardMembers, Users, Boards
from Trello.members.forms import AddAMemberForm
from flask_login import current_user
from Trello import db, es
from Trello.authorization import third_level_authorization, second_level_authorization
from Trello.services import fetch_all_board_members_info, check_if_search_is_enabled

members = Blueprint("members", __name__)
search_enabled = check_if_search_is_enabled()


def get_all_add_user_options(members_name):
    add_user_options = []
    all_users = Users.query.all()
    for user in all_users:
        if user.username not in members_name:
            add_user_options.append((user.id, user.username))

    return add_user_options


def add_a_member_to_a_board_in_database(form, board):
    board_member = BoardMembers(
        member_id=Users.query.filter_by(id=form.add_member.data).first().id,
        board_id=board,
        is_admin=False,
    )
    db.session.add(board_member)
    db.session.commit()


def get_board_members_id_in_array(board_id):
    array_of_all_member_id = []
    all_board_members_info, board_members_name = fetch_all_board_members_info(board_id)
    for board_member in all_board_members_info:
        print(board_member["board_member_info"])
        array_of_all_member_id.append(board_member["board_member_info"].id)

    return array_of_all_member_id


def add_a_member_to_a_board_in_es(form, board):
    es_board = es.get(index="boards", id=int(board))
    es_board["_source"]["members"] = get_board_members_id_in_array(board)
    body = es_board["_source"]
    es.index(index="boards", id=int(board), body=body)
    update_all_cards_of_the_board(board)


def update_all_cards_of_the_board(board_id):
    members_array = str(get_board_members_id_in_array(board_id))
    print(members_array, "members array  ")
    query = {
        "script": {
            "inline": f"ctx._source['members']={members_array}",
            "lang": "painless",
        },
        "query": {"match": {"board_id": board_id}},
    }
    es.update_by_query(index="cards", body=query)


@members.route("/<user>/boards/<board>/members", methods=["GET", "POST"])
def board_members(user, board):
    can_edit = False
    the_board = Boards.query.filter_by(id=board).first()
    if second_level_authorization(the_board.id):
        form = AddAMemberForm()
        if request.method == "POST":
            if form.add_member.data != "None":
                add_a_member_to_a_board_in_database(form, board)
                if search_enabled:
                    add_a_member_to_a_board_in_es(form, board)
                return redirect(
                    url_for("members.board_members", user=user, board=board)
                )
        all_board_members_info, board_members_name = fetch_all_board_members_info(board)
        add_members_name = get_all_add_user_options(board_members_name)
        form.add_member.choices = add_members_name
        board = Boards.query.filter_by(id=board).first()
        if third_level_authorization(the_board.id):
            can_edit = True

        return render_template(
            "members.html",
            members=all_board_members_info,
            form=form,
            board=board,
            can_edit=can_edit,
        )
    else:
        abort(400)


def delete_board_member_from_database(response):
    board_member = BoardMembers.query.filter_by(
        member_id=response["userId"], board_id=response["boardId"]
    ).first()
    db.session.delete(board_member)
    db.session.commit()


def delete_board_member_from_es(response):
    es_board = es.get(index="boards", id=int(response["boardId"]))
    es_board["_source"]["members"].remove(response["userId"])
    body = es_board["_source"]
    es.index(index="boards", id=int(response["boardId"]), body=body)
    print(es_board["_source"]["members"])
    update_all_cards_of_the_board(response["boardId"])


@members.route("/<user>/boards/<board>/members/remove-board-member", methods=["DELETE"])
def delete_board_member(user, board):
    if third_level_authorization(board):
        response = request.get_json()
        delete_board_member_from_database(response)
        if search_enabled:
            delete_board_member_from_es(response)
        return "ok"
    else:
        abort(400)

