from Trello.models import Users, BoardMembers
from flask_login import current_user
from Trello.models import Boards
import os


def fetch_all_board_members_info(board_id):
    board_members_name = []
    all_board_members = BoardMembers.query.filter_by(board_id=board_id).all()
    all_board_members_info = []
    for board_member in all_board_members:
        user_info = Users.query.filter_by(id=board_member.member_id).first()
        member_info = {"board_member": board_member, "board_member_info": user_info}
        board_members_name.append(user_info.username)
        all_board_members_info.append(member_info)
    return all_board_members_info, board_members_name


def fetch_all_shared_boards(user_id):
    all_shared_board_relations = BoardMembers.query.filter_by(
        member_id=user_id, is_admin=False
    ).all()
    all_shared_boards = []
    print(all_shared_board_relations)
    for shared_board in all_shared_board_relations:
        all_shared_boards.append(
            Boards.query.filter_by(id=shared_board.board_id).first()
        )
    return all_shared_boards


def check_if_search_is_enabled():
    if os.getenv("SEARCH_ENABLED") == "TRUE":
        return True
    else:
        return False
